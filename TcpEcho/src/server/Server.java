package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {

	//	static int port=7;
	static int port=10500;
	static ServerSocket echo = null;

	public static void main( String args[] ) throws IOException, InterruptedException {
		if ( args.length > 0 ) {
			port= new Integer(args[0]);
		}
		echo = new ServerSocket(port);
		Thread t = new Server();
		t.start();
	}

	public void run() {
		System.out.format("RFC 862 ECHO server now running (on port %d)%n",port);
		while(true)
		{
			Socket connection = null;
			try {
				connection = echo.accept();
			} catch (IOException e) {
				System.err.println("Fallisce la accept");
				return;
			}
			try {
				System.out.println("Connection from client");
				BufferedReader in = 
						new BufferedReader(
								new InputStreamReader(connection.getInputStream()));
				PrintWriter out = 
						new PrintWriter(connection.getOutputStream(),true);
				String data;
				while ((data = in.readLine()) != null){
					out.println(data);
				}
				in.close();
			} catch ( IOException e ) {
				System.err.println("Errore di comunicazione");
			} finally {
				try {
					connection.close();
				} catch (IOException e) {
				}
			}
		}
	}
}
